package com.devcamp.s54.restapi;



import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@CrossOrigin

public class welcome {
   
    @GetMapping("/devcamp-welcome")
    public String nice() {
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Date now= new Date();
        return String.format("Have a nice day. It is %s!.", dateFormat.format(now));
    }
}

